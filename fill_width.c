/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_width.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: itsuman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 17:42:10 by itsuman           #+#    #+#             */
/*   Updated: 2017/02/24 17:59:22 by itsuman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		is_f(t_strf *f, char c)
{
	int	i;

	i = 0;
	while (i < 5)
	{
		if (f->flag[i] == c)
			return (1);
		i++;
	}
	return (0);
}

char	*fill(char *s, t_strf *f, int t, char c)
{
	int		i;
	char	a[2];
	int		l;

	l = ft_strlen(s);
	i = f->width;
	a[1] = '\0';
	a[0] = c;
	if (is_f(f, '-'))
		t = 0;
	while (i - l > 0)
	{
		if (t == 1)
			s = ft_strjoin(a, s);
		else
			s = ft_strjoin(s, a);
		i--;
	}
	return (s);
}

char	*f_alt(t_strf *f, char *s)
{
	int		i;
	char	s1[3];

	i = 0;
	s1[0] = '0';
	s1[1] = f->type;
	s1[2] = '\0';
	if ((f->type == 'o' || f->type == 'O') && s[0] != '0')
		s = ft_strjoin("0", s);
	else if (f->type == 'x' || f->type == 'X')
		s = ft_strjoin(s1, s);
	if (is_f(f, '-'))
		return (fill(s, f, 0, ' '));
	else if (is_f(f, '0') && !(is_f(f, '-')))
		return (fill(s, f, 1, '0'));
	return (fill(s, f, 1, ' '));
}

char	*f1(char *s, t_strf *f)
{
	if (is_f(f, '#') && (f->type == 'o' ||
				f->type == 'x' || f->type == 'O' || f->type == 'X'))
		return (f_alt(f, s));
	else if (is_f(f, '-'))
		return (fill(s, f, 0, ' '));
	else if (is_f(f, '0'))
		return (fill(s, f, 1, '0'));
	return (fill(s, f, 1, ' '));
}

char	*fill_width(char *s, t_strf *f)
{
	int	i;
	int	l;

	i = 0;
	l = ft_strlen(s);
	if (f->width <= l && (!(is_f(f, ' ')) ||
				(is_f(f, ' ') && s[0] == '-')) && !(is_f(f, '#')))
		return (s);
	else if (f->width <= l && is_f(f, ' ') && s[0] != '-' && !(is_f(f, '#')))
		return (ft_strjoin(" ", s));
	else if (f->width > l)
		return (f1(s, f));
	else if (is_f(f, '#') && (f->type == 'o' ||
				f->type == 'x' || f->type == 'O' || f->type == 'X'))
		return (f_alt(f, s));
	return (s);
}
