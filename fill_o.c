/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_o.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: itsuman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 21:45:18 by itsuman           #+#    #+#             */
/*   Updated: 2017/02/24 17:50:54 by itsuman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*itoa_base(unsigned long long n, char type)
{
	unsigned long long 	base;
	int					var;
	static char			*s;
	char				a[2];

	base = (type == 'o') || (type == 'O') ? 8 : 16;
	var = (type == 'o') || (type == 'x' || type == 'p') ? 97 : 65;
	s = "";
	a[1] = '\0';
	if (n >= base)
		itoa_base(n / base, type);
	if (n % base < 10)
	{
		n = (n % base) + 48;
		a[0] = n;
		s = ft_strjoin(s, a);
	}
	else
	{
		n = (n % base) + (var - 10);
		a[0] = n;
		s = ft_strjoin(s, a);
	}
	return (s);
}

char	*fill_ox(t_strf *f, va_list ap)
{
	if (f->mod_size == 'z')
		return (itoa_base((unsigned long long)(va_arg(ap, ssize_t)), f->type));
	else if (f->mod_size == 'j')
		return (itoa_base((unsigned long long)(va_arg(ap, intmax_t)), f->type));
	else if (f->mod_size == 'l' && f->pr == 4)
		return (itoa_base((unsigned long long)(va_arg(ap, long long)),
					f->type));
	else if ((f->mod_size == 'l' && f->pr == 3) || f->type == 'D')
		return (itoa_base((unsigned long long)(va_arg(ap, long)), f->type));
	else if (f->mod_size == 'h' && f->pr == 2)
		return (itoa_base((short int)(va_arg(ap, int)), f->type));
	else if (f->mod_size == 'h' && f->pr == 1)
		return (itoa_base((signed char)(va_arg(ap, int)), f->type));
	return (itoa_base((unsigned long long)(va_arg(ap, long int)), f->type));
}
