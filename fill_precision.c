/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fill_precision.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: itsuman <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 19:12:02 by itsuman           #+#    #+#             */
/*   Updated: 2017/02/24 17:53:51 by itsuman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*get_str(char *s, int pr)
{
	int		i;
	char	a[2];
	char	*str;

	i = 0;
	a[1] = '\0';
	str = "";
	if (pr > 0)
	{
		while (pr > 0 && s[i])
		{
			a[0] = s[i];
			str = ft_strjoin(str, a);
			i++;
			pr--;
		}
		return (str);
	}
	return (s);
}

char			*fill_precision(char *s, t_strf *f)
{
	int		l;
	char	*a;
	int		pr;

	l = ft_strlen(s);
	a = "";
	pr = f->precision;
	if (l >= pr && f->type != 's' && f->type != 'S' && f->type != 'c'
			&& f->type != 'C')
		return (s);
	else if (l <= pr && f->type != 's' && f->type != 'S' && f->type != 'c'
			&& f->type != 'C')
	{
		while ((pr - l) > 0)
		{
			a = ft_strjoin(a, "0");
			pr--;
		}
		s = ft_strjoin(a, s);
	}
	else
		return (get_str(s, pr));
	return (s);
}
